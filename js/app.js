document.addEventListener("DOMContentLoaded", () => {
  const herramientas = [
    {
      id: 1,
      accion: "seleccionar-poligono",
      icono: ["fas", "fa-draw-polygon"],
      descripcion: "Selecciona un polígono en la imagen",
      activo: false,
    },
    {
      id: 2,
      accion: "seleccionar-cuadrado",
      icono: ["fas", "fa-vector-square"],
      descripcion: "Selecciona un rectangulo",
      activo: false,
    },
    {
      id: 3,
      accion: "borrador",
      icono: ["fas", "fa-eraser"],
      descripcion: "Borra una parte de la imagen",
      activo: false,
    },
    {
      id: 4,
      accion: "llenar",
      icono: ["fas", "fa-fill-drip"],
      descripcion: "Llena un área con el color seleccionado",
      activo: false,
    },
    {
      id: 5,
      accion: "seleccionar-color",
      icono: ["fas", "fa-eye-dropper"],
      descripcion: "Captura un color de la imagen",
      activo: false,
    },
    {
      id: 6,
      accion: "zoom",
      icono: ["fas", "fa-search"],
      descripcion: "Cambia el nivel de zoom",
      activo: false,
    },
    {
      id: 7,
      accion: "lapiz",
      icono: ["fas", "fa-pencil-alt"],
      descripcion: "Dibuja una figura libre de un pixel de ancho",
      activo: false,
    },
    {
      id: 8,
      accion: "pincel",
      icono: ["fas", "fa-brush"],
      descripcion: "Dibuja una figura con un pincel",
      activo: false,
    },
    {
      id: 9,
      accion: "aerosol",
      icono: ["fas", "fa-spray-can"],
      descripcion: "Dibuja una figura libre con aerosol",
      activo: false,
    },
    {
      id: 10,
      accion: "texto",
      icono: ["fab", "fa-amilia"],
      descripcion: "Inserta un texto",
      activo: false,
    },
    {
      id: 11,
      accion: "dibujar-linea",
      icono: ["fas", "fa-minus"],
      descripcion: "Dibuja una linea recta",
      activo: true,
    },
    {
      id: 12,
      accion: "dibujar-curva",
      icono: ["fas", "fa-bezier-curve"],
      descripcion: "Dibuja una curva",
      activo: false,
    },
    {
      id: 13,
      accion: "dibujar-cuadrado",
      icono: ["far", "fa-square"],
      descripcion: "Dibuja un rectangulo",
      activo: false,
    },
    {
      id: 14,
      accion: "dibujar-poligono",
      icono: ["fas", "fa-draw-polygon"],
      descripcion: "Dibuja un polígono",
      activo: false,
    },
    {
      id: 15,
      accion: "dibujar-elipse",
      icono: ["far", "fa-circle"],
      descripcion: "Dibuja una elipse",
      activo: false,
    },
    {
      id: 16,
      accion: "dibujar-cuadrado-redondo",
      icono: ["far", "fa-square"],
      descripcion: "Dibuja un rectángulo con esquina redondeadas",
      activo: false,
    },
  ];
  let svg = document.getElementById("svg");
  let zonaCentral = document.getElementById("zona-central");
  let zonaIzquierda = document.getElementById("zona-izquierda");
  let colorActualLinea = "#000";
  let colorActualRelleno = "#fff";
  let coordenada1 = null;
  let coordenada2 = null;
  let coordenadas = [];

  // La acción por defecto al cargar la página es "Dibujar línea"
  let accionActual = herramientas[10].accion;

  const crearLinea = () => {
    const linea = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "line"
    );
    linea.setAttribute("x1", coordenada1.x);
    linea.setAttribute("y1", coordenada1.y);
    linea.setAttribute("x2", coordenada2.x);
    linea.setAttribute("y2", coordenada2.y);
    linea.setAttribute("stroke-width", 3);
    linea.setAttribute("stroke", colorActualLinea);
    // Agrega la linea al SVG
    svg.appendChild(linea);
  };

  const crearCirculo = () => {
    // Crea un círculo
    const circulo = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "circle"
    );
    // Calcula el radio del círculo
    const radio = Math.sqrt(
      Math.pow(coordenada2.x - coordenada1.x, 2) +
        Math.pow(coordenada2.y - coordenada1.y, 2)
    );
    circulo.setAttribute("cx", coordenada1.x);
    circulo.setAttribute("cy", coordenada1.y);
    circulo.setAttribute("r", radio);
    circulo.setAttribute("stroke-width", "3");
    circulo.setAttribute("fill", colorActualRelleno);
    circulo.setAttribute("stroke", colorActualLinea);
    // Agrega el circulo al SVG
    svg.appendChild(circulo);
  };

  const crearRectangulo = () => {
    // Crea un rectángulo
    const rectangulo = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "rect"
    );

    // Calcula la coordenada principal, alto y ancho del rectángulo
    let x, y, ancho, alto;
    if (coordenada1.x < coordenada2.x && coordenada1.y < coordenada2.y) {
      x = coordenada1.x;
      y = coordenada1.y;
      ancho = coordenada2.x - coordenada1.x;
      alto = coordenada2.y - coordenada1.y;
    } else if (coordenada1.x > coordenada2.x && coordenada1.y < coordenada2.y) {
      x = coordenada2.x;
      y = coordenada1.y;
      ancho = coordenada1.x - coordenada2.x;
      alto = coordenada2.y - coordenada1.y;
    } else if (coordenada1.x > coordenada2.x && coordenada1.y > coordenada2.y) {
      x = coordenada1.x;
      y = coordenada2.y;
      ancho = coordenada1.x - coordenada2.x;
      alto = coordenada1.y - coordenada2.y;
    } else if (coordenada1.x < coordenada2.x && coordenada1.y > coordenada2.y) {
      x = coordenada1.x;
      y = coordenada2.y;
      ancho = coordenada2.x - coordenada1.x;
      alto = coordenada1.y - coordenada2.y;
    }
    // configura el rectángulo
    rectangulo.setAttribute("x", x);
    rectangulo.setAttribute("y", y);
    rectangulo.setAttribute("width", ancho);
    rectangulo.setAttribute("height", alto);
    rectangulo.setAttribute("stroke-width", "3");
    rectangulo.setAttribute("fill", colorActualRelleno);
    rectangulo.setAttribute("stroke", colorActualLinea);
    // Agrega el circulo al SVG
    svg.appendChild(rectangulo);
  };

  const crearRectanguloRedondo = () => {
    // Crea un rectángulo
    const rectangulo = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "rect"
    );

    // Calcula la coordenada principal, alto y ancho del rectángulo
    let x, y, ancho, alto;
    if (coordenada1.x < coordenada2.x && coordenada1.y < coordenada2.y) {
      x = coordenada1.x;
      y = coordenada1.y;
      ancho = coordenada2.x - coordenada1.x;
      alto = coordenada2.y - coordenada1.y;
    } else if (coordenada1.x > coordenada2.x && coordenada1.y < coordenada2.y) {
      x = coordenada2.x;
      y = coordenada1.y;
      ancho = coordenada1.x - coordenada2.x;
      alto = coordenada2.y - coordenada1.y;
    } else if (coordenada1.x > coordenada2.x && coordenada1.y > coordenada2.y) {
      x = coordenada1.x;
      y = coordenada2.y;
      ancho = coordenada1.x - coordenada2.x;
      alto = coordenada1.y - coordenada2.y;
    } else if (coordenada1.x < coordenada2.x && coordenada1.y > coordenada2.y) {
      x = coordenada1.x;
      y = coordenada2.y;
      ancho = coordenada2.x - coordenada1.x;
      alto = coordenada1.y - coordenada2.y;
    }
    // configura el rectángulo
    rectangulo.setAttribute("x", x);
    rectangulo.setAttribute("y", y);
    rectangulo.setAttribute("rx", 10);
    rectangulo.setAttribute("ry", 10);
    rectangulo.setAttribute("width", ancho);
    rectangulo.setAttribute("height", alto);
    rectangulo.setAttribute("stroke-width", "3");
    rectangulo.setAttribute("fill", colorActualRelleno);
    rectangulo.setAttribute("stroke", colorActualLinea);
    // Agrega el circulo al SVG
    svg.appendChild(rectangulo);
  };

  const crearTexto = (texto) => {
    // Crea un texto
    const text = document.createElementNS("http://www.w3.org/2000/svg", "text");
    text.setAttribute("x", coordenada1.x);
    text.setAttribute("y", coordenada1.y);
    text.setAttribute("font-size", "2.5em");
    text.setAttribute("fill", colorActualRelleno);
    text.setAttribute("stroke", colorActualLinea);
    text.textContent = texto;
    // Agrega el texto al SVG
    svg.appendChild(text);
  };

  const crearPoligono = () => {
    // Crea un texto
    const poligono = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "polygon"
    );
    // Obtiene los puntos del poligono
    const puntos = coordenadas.map((c) => `${c.x},${c.y}`).join(" ");
    poligono.setAttribute("points", puntos);
    poligono.setAttribute("stroke-width", 3);
    poligono.setAttribute("fill", colorActualRelleno);
    poligono.setAttribute("stroke", colorActualLinea);
    // Agrega el poligono al SVG
    svg.appendChild(poligono);
    // Reinicializa las coordenadas
    coordenadas = [];
  };

  svg.addEventListener("mousemove", (evt) => {
    // Obtiene el SVG
    const e = evt.target;
    const dim = e.getBoundingClientRect();
    // Obtiene X y Y
    const x = evt.clientX - dim.left;
    const y = evt.clientY - dim.top;
    // Muestra las coordenadas en la barra de estado
    zonaCentral.textContent = `${x},${y}`;
  });

  svg.addEventListener("click", (e) => {
    // Crea un punto en el SVG
    const pt = svg.createSVGPoint();

    // Pasa las coordenadas al punto SVG
    pt.x = e.clientX;
    pt.y = e.clientY;

    // Transforma las coordenadas de mouse a coordenadas SVG
    const svgP = pt.matrixTransform(svg.getScreenCTM().inverse());

    // Almacena las coordenadas
    const x = svgP.x;
    const y = svgP.y;

    // CREAR TEXTO
    if (accionActual === herramientas[9].accion) {
      if (coordenada1 === null) {
        coordenada1 = { x, y };
        // PIde el texto al usuario
        const texto = prompt("Digite el texto");
        if (texto !== null && texto !== "") {
          // Crea el texto
          crearTexto(texto);
        }
        // Reinicializa las coordenadas
        coordenada1 = null;
        coordenada2 = null;
      }
      // CREAR LINEA
    } else if (accionActual === herramientas[10].accion) {
      if (coordenada1 === null) {
        coordenada1 = { x, y };
      } else if (coordenada2 === null) {
        coordenada2 = { x, y };
        // Crea una línea
        crearLinea();
        // Reinicializa las coordenadas
        coordenada1 = null;
        coordenada2 = null;
      }

      // CREAR RECTANGULO
    } else if (accionActual === herramientas[12].accion) {
      if (coordenada1 === null) {
        coordenada1 = { x, y };
      } else if (coordenada2 === null) {
        coordenada2 = { x, y };
        crearRectangulo();
        // Reinicializa las coordenadas
        coordenada1 = null;
        coordenada2 = null;
      }

      // CREAR POLIGONO
    } else if (accionActual === herramientas[13].accion) {
      // Almacena la coordenada actual
      coordenadas.push({ x, y });

      // CREAR CIRCULO
    } else if (accionActual === herramientas[14].accion) {
      if (coordenada1 === null) {
        coordenada1 = { x, y };
      } else if (coordenada2 === null) {
        coordenada2 = { x, y };
        crearCirculo();
        // Reinicializa las coordenadas
        coordenada1 = null;
        coordenada2 = null;
      }
      // CREAR RECTANGULO REDONDO
    } else if (accionActual === herramientas[15].accion) {
      if (coordenada1 === null) {
        coordenada1 = { x, y };
      } else if (coordenada2 === null) {
        coordenada2 = { x, y };
        crearRectanguloRedondo();
        // Reinicializa las coordenadas
        coordenada1 = null;
        coordenada2 = null;
      }
    }
  });

  svg.addEventListener("dblclick", (e) => {
    // Al hacer doble click se almacenan las coordenadas de ambos clicks y
    // por lo tanto, se elimina la última coordenada
    coordenadas.pop();

    // CREAR POLIGONO
    if (accionActual === herramientas[13].accion) {
      // Crea el poligono
      crearPoligono();
    }
  });

  const clickHerramienta = (e) => {
    // Verifica si se hizo click en el botón o en su ícono
    let objeto;
    if (e.target !== e.currentTarget) {
      objeto = e.currentTarget;
    } else {
      objeto = e.target;
    }
    // Guarda las acciones globales de la herramienta
    accionActual = objeto.getAttribute("data-accion");
    zonaIzquierda.textContent = objeto.getAttribute("data-descripcion");

    // Reinicializa las coordenadas
    coordenada1 = null;
    coordenada2 = null;

    // Elimina la clase "activo" de todos lod botones de herramientas
    const divHerramientas = document.getElementById("herramientas");
    Array.from(divHerramientas.children).forEach((div) => {
      div.classList.remove("boton-activo");
    });

    // Asigna la clase "boton-activo" al botón al que se le hizo click
    objeto.classList.add("boton-activo");
  };

  const generarHerramientas = () => {
    // Obtiene el DIV de la barra de herramientas
    const divHerramientas = document.getElementById("herramientas");
    // Recorre las herramientas
    herramientas.forEach((herramienta) => {
      // Crea el DIV de la herramienta
      const div = document.createElement("div");
      div.classList.add("boton");
      if (herramienta.activo) {
        div.classList.add("boton-activo");
      }
      div.setAttribute("data-accion", herramienta.accion);
      div.setAttribute("data-descripcion", herramienta.descripcion);
      const i = document.createElement("i");
      i.classList.add(herramienta.icono[0]);
      i.classList.add(herramienta.icono[1]);
      div.appendChild(i);
      div.addEventListener("click", clickHerramienta);
      // Agrega la herramienta a la barra
      divHerramientas.appendChild(div);
    });
  };

  const cambiarColorLinea = (e) => {
    // Obtiene el DIV al que se le hizo click
    const div = e.target;
    // Obtiene el DIV de color actual
    const divColorActualLinea = document.getElementById("color-actual-linea");
    // Obtiene el color del DIV
    colorActualLinea = div.style.background;
    divColorActualLinea.style.background = colorActualLinea;
  };

  const cambiarColorRelleno = (e) => {
    // Evita que se muestre el menú contextual
    e.preventDefault();
    // Obtiene el DIV al que se le hizo click
    const div = e.target;
    // Obtiene el DIV de color actual
    const divColorActualRelleno = document.getElementById(
      "color-actual-relleno"
    );
    // Obtiene el color del DIV
    colorActualRelleno = div.style.background;
    divColorActualRelleno.style.background = colorActualRelleno;
  };

  const generarPaleta = () => {
    const colores = [
      "#010101",
      "#807d7c",
      "#810007",
      "#7d8027",
      "#008226",
      "#00867e",
      "#0c0675",
      "#7C0776",
      "#85824C",
      "#004140",
      "#0085F2",
      "#013E7C",
      "#4718E2",
      "#864217",
      "#ffffff",
      "#C0C0C2",
      "#FA0018",
      "#F8FC53",
      "#00FC50",
      "#00FEFC",
      "#1F18F1",
      "#FF0FF5",
      "#FCFA91",
      "#00FC8D",
      "#7DF6F7",
      "#7E7DF5",
      "#FA0076",
      "#FF8354",
    ];
    // Obtiene la paleta
    const paleta = document.getElementById("paleta");
    // Recorre los colores y agrega el cuadro a al paleta
    colores.forEach((color) => {
      // Crea el DIV de color
      const div = document.createElement("div");
      div.style.background = color;
      // Asocia la función para cambiar el color de la linea
      div.addEventListener("click", cambiarColorLinea);
      // Asocia la función para cambiar el color de relleno
      div.addEventListener("contextmenu", cambiarColorRelleno);
      // Agrega el DIV a la paleta
      paleta.appendChild(div);
    });
  };

  // Generar la paleta
  generarPaleta();

  // Generar las herramientas de dibujo
  generarHerramientas();
});
