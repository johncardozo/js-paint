# Paint en Javascript

Ejemplo del uso de SVG para dibujar imágenes con base en primitivas usando HTML, CSS y Javascript.

Este ejemplo usa:

- HTML
- CSS
  - CSS Grid
  - Flex
- Javascript vanilla
- SVG
